#include "font.h"

#define CHAR_A  0
#define CHAR_N 13
#define CHAR_O 14
#define CHAR_COLON 26
#define CHAR_0 27
#define CHAR_9 36
#define CHAR_SPACE 37

Sprite Font::chars[CHAR_TOTAL];

void Font::init(const Sprite& src) {
  for (int i = 0; i <= CHAR_N; ++i)
    chars[i] = Sprite(i*9, 11, 8, 8, src);

  for (int i = 0; i <= CHAR_COLON - CHAR_N; ++i)
    chars[CHAR_O+i] = Sprite(i*9, 20, 8, 8, src);

  for (int i = 0; i <= CHAR_9 - CHAR_COLON; ++i)
    chars[CHAR_0+i] = Sprite(i*9, 29, 8, 8, src);
}

void Font::draw(const std::string& text, int x, int y) {
  for (size_t i = 0; i < text.size(); ++i) {
    int n = Font::getChar(text[i]);
    chars[n].draw(x+i*8, y);
  }
}

int Font::getChar(char c) {
  if      ('a' <= c && c <= 'z')
    return CHAR_A + (c - 'a');

  else if ('A' <= c && c <= 'Z')
    return CHAR_A + (c - 'A');

  else if ('0' <= c && c <= '9')
    return CHAR_0 + (c - '0');

  else if (c == ':')
    return CHAR_COLON;

  return CHAR_SPACE;
}