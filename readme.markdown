# README
## About

This project is a Tetris clone I made in November of 2012.

It uses SDL and SDL_image libraries in its version 1.2 and openGL for rendering.

The project has a .vcxproj for compiling in Windows (with all the needed libs in the libs folder) and a .pro for compiling it in Linux with qmake.
