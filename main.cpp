#include "engine/mode_manager.h"
#include "engine/video/window.h"
#include "engine/time_manager.h"
#include "engine/input_manager.h"
#include "engine/system_manager.h"
#include "engine/log.h"

#include "modes/play_mode.h"
#include "modes/tetromino.h"

#include "constants.h"
#include "font.h"

// Delete console
#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"")

void init() {
  new Window(WINDOW_WIDTH, WINDOW_HEIGHT);
  new SystemHandler;
  new ModeHandler;
  new TimeHandler;
  new InputHandler;

  Sprite gfx("gfx.png");
  Block::initSprites(gfx);
  Font::init(gfx);

  FILELog::ReportingLevel() = logERROR;

  AppWindow->setTitle("Tetris");
  Camera* c = AppWindow->getCamera();
  c->zoom(ZOOM);
  ModeManager->push(new PlayMode);
}

void quit() {
  delete InputManager;
  delete TimeManager;
  delete ModeManager;
  delete SystemManager;
  delete AppWindow;
}

int main() {
  init();

  bool finished = false;
  while (!finished) {
    TimeManager->update();
    InputManager->update();
    ModeManager->update();

    AppWindow->clear();
    ModeManager->draw();

    AppWindow->update();

    finished = SystemManager->isFinished();
  }
  
  quit();
}