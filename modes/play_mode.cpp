#include "play_mode.h"
#include <sstream>
#include "tetromino.h"
#include "engine/mode_manager.h"
#include "engine/input_manager.h"
#include "engine/system_manager.h"
#include "engine/video/shapes.h"
#include "engine/log.h"
#include "constants.h"
#include "font.h"
#include "game_stopped.h"

BlockGrid::BlockGrid() {
  grid = std::vector<std::vector<Block> >(GRID_HEIGHT, std::vector<Block>(GRID_WIDTH));
}

void BlockGrid::draw() {
  for (int y = 0; y < drawHeight(); ++y)
    for (int x = 0; x < drawWidth(); ++x)
      grid[y][x].sprite.draw(x*BLOCK_WIDTH,y*BLOCK_HEIGHT);
  drawBorders();
  drawIntersections();
}

int BlockGrid::drawWidth() const {
  return width();
}

int BlockGrid::drawHeight() const {
  return grid.size() - 2; // Two upper lines are not visible
}

bool BlockGrid::empty(int x, int y) const {
  return !grid[y][x].sprite.initialized();
}

bool BlockGrid::insert(Tetromino& t) {
  t.attachGrid(this);
  t.setPosition(RESPAWN_X, RESPAWN_Y);

  std::vector<Block> blocks = t.getBlocks();
  for (size_t i = 0; i < blocks.size(); ++i)
    if (!empty(blocks[i].x, blocks[i].y))
      return false;

  for (size_t i = 0; i < blocks.size(); ++i)
    grid[blocks[i].y][blocks[i].x] = blocks[i];

  return true;
}

int BlockGrid::checkLines(Tetromino& t) {
  std::vector<Block> v = t.getBlocks();
  int lines = 0;
  for (size_t i = 0; i < v.size(); ++i) {
    if (lineCompleted(v[i].y)) {
      ++lines;
      removeLine(v[i].y);
    }
  }
  return lines;
}

void BlockGrid::moveBlocks(const std::vector<Block>& src,
                           const std::vector<Block>& dst) {
  // At this point, we checked that the dst blocks are all empty, so we
  // can just remove the blocks from src and put them in src.
  for (size_t i = 0; i < src.size(); ++i)
    grid[src[i].y][src[i].x] = Block();
  for (size_t i = 0; i < dst.size(); ++i)
    grid[dst[i].y][dst[i].x] = src[i];
}

bool BlockGrid::validMove(const std::vector<Block>& src,
                          const std::vector<Block>& dst) const {
  for (size_t i = 0; i < dst.size(); ++i) {
    if (dst[i].x < 0 || dst[i].x >= width())
      return false;
    if (dst[i].y < 0 || dst[i].y >= height())
      return false;
    if (!isInside(dst[i], src) && !empty(dst[i].x, dst[i].y))
      return false;
  }
  return true;
}

bool BlockGrid::lineCompleted(int y) {
  for (size_t i = 0; i < grid[y].size(); ++i)
    if (empty(i, y))
      return false;
  return true;
}

void BlockGrid::removeLine(int y) {
  for (size_t i = y; i < grid.size()-1; ++i)
    grid[i] = grid[i+1];
  grid[grid.size()-1] = std::vector<Block>(width());
}

bool BlockGrid::isInside(const Block& b, const std::vector<Block>& dst) const {
  for (size_t i = 0; i < dst.size(); ++i)
    if (b.x == dst[i].x && b.y == dst[i].y)
      return true;
  return false;
}

void BlockGrid::drawBorders() {
  static Point p0(0,0);
  static Point p1(drawWidth()*BLOCK_WIDTH, drawHeight()*BLOCK_HEIGHT);
  Rect::drawBorders(p0, p1);
}

void BlockGrid::drawIntersections() {
  for (int i = 0; i < drawWidth(); ++i)
    for (int j = 0; j < drawHeight(); ++j)
      Point::draw(i*BLOCK_WIDTH, j*BLOCK_HEIGHT);
}

PlayMode::PlayMode() : score(0) {
  random.randomize();
}

PlayMode::~PlayMode()
{ }

void PlayMode::enter() {
  generateNextTetromino();
  respawnNextTetromino();
  generateNextTetromino();

  InputManager->setRepeatDelay(PLAY_MODE_REPEAT_DELAY);
  timer.setDuration(BLOCK_FALL_MS);
  timer.run();
}

void PlayMode::pause() {
  timer.pause();
}

void PlayMode::resume() {
  InputManager->setRepeatDelay(PLAY_MODE_REPEAT_DELAY);
  timer.run();
}

void PlayMode::update() {
  if (InputManager->isPressed(KEY_UP, STICKY))
    turnRight();
  if (InputManager->isPressed(KEY_DOWN, REPEAT)) {
    moveDown();
    timer.reset();
  }
  if (InputManager->isPressed(KEY_LEFT, REPEAT))
    moveLeft();
  if (InputManager->isPressed(KEY_RIGHT, REPEAT))
    moveRight();

  if (InputManager->isPressed(KEY_PAUSE, STICKY))
    ModeManager->push(new GamePaused(this));
  if (InputManager->isPressed(KEY_EXIT))
    SystemManager->stop();

  timer.update();
  if (timer.isFinished()){
    moveDown();
    timer.reset();
  }
}

void PlayMode::draw() {
  grid.draw();
  drawNextTetromino();
  drawScore();
}

void PlayMode::generateNextTetromino() {
  int type = random.getInt(TETROMINO_TOTAL);
  next = Tetromino(type);
  next.setPosition(NEXT_TETROMINO_DRAW_POS_X, NEXT_TETROMINO_DRAW_POS_Y);
}

void PlayMode::respawnNextTetromino() {
  current = next;
  if (!grid.insert(current))
    ModeManager->push(new GameOver(this));
}

void PlayMode::turnRight() {
  current.turnRight();
}

void PlayMode::moveRight() {
  current.moveRight();
}

void PlayMode::moveLeft() {
  current.moveLeft();
}

void PlayMode::moveDown() {
  if (!current.moveDown()) {
    int lines = grid.checkLines(current);
    addScore(lines);

    respawnNextTetromino();
    generateNextTetromino();
  }
}

void PlayMode::addScore(int lines) {
  if (lines > 0)
    score += LINE_SCORE*lines + LINE_BONUS*(lines-1);
}

std::string PlayMode::getStringScore() {
  std::stringstream ss;
  ss << score;
  return ss.str();
}

void PlayMode::drawNextTetromino() {
  std::vector<Block> blocks = next.getBlocks();
  for (size_t i = 0; i < blocks.size(); ++i)
    blocks[i].sprite.draw(blocks[i].x * BLOCK_WIDTH, blocks[i].y * BLOCK_HEIGHT);
  static Point p0((NEXT_TETROMINO_DRAW_POS_X-2)*BLOCK_WIDTH,
                  (NEXT_TETROMINO_DRAW_POS_Y-1)*BLOCK_HEIGHT);
  static Point p1((NEXT_TETROMINO_DRAW_POS_X+4)*BLOCK_WIDTH,
                  (NEXT_TETROMINO_DRAW_POS_Y+3)*BLOCK_HEIGHT);
  Rect::drawBorders(p0, p1);
}

void PlayMode::drawScore() {
  Font::draw("score:",         SCORE_TEXT_DRAW_POS_X, SCORE_TEXT_DRAW_POS_Y);
  Font::draw(getStringScore(), SCORE_DRAW_POS_X, SCORE_DRAW_POS_Y);
}