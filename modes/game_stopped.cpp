#include "game_stopped.h"
#include "engine/video/window.h"
#include "engine/video/shapes.h"
#include "engine/mode_manager.h"
#include "engine/input_manager.h"
#include "engine/system_manager.h"
#include "play_mode.h"
#include "constants.h"
#include "font.h"

void GameStopped::shadowScreen() {
  static Point p0(0, 0);
  static Point p1(AppWindow->getWidth(), AppWindow->getHeight());
  Rect::draw(p0, p1, 0, 0, 0, 128);
}

void GameOver::update() {
  if (InputManager->isPressed(KEY_EXIT))
    SystemManager->stop();
  if (InputManager->isPressed(KEY_PAUSE, STICKY)) {
    ModeManager->pop();
    ModeManager->pop();
    ModeManager->push(new PlayMode);
  }
}

void GameOver::draw() {
  parent->draw();
  shadowScreen();
  Font::draw("GAME OVER", GAME_OVER_DRAW_POS_X, GAME_OVER_DRAW_POS_Y);
}

void GamePaused::update() {
  if (InputManager->isPressed(KEY_EXIT))
    SystemManager->stop();
  if (InputManager->isPressed(KEY_PAUSE, STICKY))
    ModeManager->pop();
}

void GamePaused::draw() {
  parent->draw();
  shadowScreen();
  Font::draw("PAUSE", GAME_OVER_DRAW_POS_X, GAME_OVER_DRAW_POS_Y);
}