#ifndef TETROMINO_H
#define TETROMINO_H

#include <vector>
#include "engine/video/sprite.h"

class BlockGrid;

struct Block {
  static Sprite texture;
  static void initSprites(const Sprite& src);
  Block() : x(0), y(0) { }
  int x, y;
  Sprite sprite;
};

enum TetrominoType {
  TETROMINO_L = 0,
  TETROMINO_REVERSED_L,
  TETROMINO_Z,
  TETROMINO_REVERSED_Z,
  TETROMINO_T,
  TETROMINO_QUAD,
  TETROMINO_LONG,

  // Used for generating a random tetromino
  TETROMINO_TOTAL
};

class Tetromino {
 public:
  Tetromino(int type = 0);
  ~Tetromino() { }

  void attachGrid(BlockGrid* grid_);
  void setPosition(int x_, int y_);
  int getX() const { return x; }
  int getY() const { return y; }


  std::vector<Block> getBlocks() const;
  std::vector<Block> getMoveRight() const;
  std::vector<Block> getMoveLeft () const;
  std::vector<Block> getMoveDown () const;
  std::vector<Block> getTurnRight() const;
  std::vector<Block> getTurnLeft () const;

  bool moveRight();
  bool moveLeft ();
  bool moveDown ();
  bool turnRight();
  bool turnLeft ();

 private:
  void genL();
  void genRevL();
  void genZ();
  void genRevZ();
  void genT();
  void genQuad();
  void genLong();
  void assignColor(Block b);

  Block blocks[4];
  int x, y;
  BlockGrid* grid;
};

#endif