#include "tetromino.h"
#include "play_mode.h"
#include "constants.h"

static Block YellowBlock;
static Block BlueBlock;
static Block OrangeBlock;
static Block RedBlock;
static Block PurpleBlock;
static Block GreenBlock;
static Block PinkBlock;

void Block::initSprites(const Sprite& src) {
  YellowBlock.sprite = Sprite( 0,0,BLOCK_WIDTH,BLOCK_HEIGHT,src);
  BlueBlock.sprite   = Sprite(11,0,BLOCK_WIDTH,BLOCK_HEIGHT,src);
  OrangeBlock.sprite = Sprite(22,0,BLOCK_WIDTH,BLOCK_HEIGHT,src);
  RedBlock.sprite    = Sprite(33,0,BLOCK_WIDTH,BLOCK_HEIGHT,src);
  PurpleBlock.sprite = Sprite(44,0,BLOCK_WIDTH,BLOCK_HEIGHT,src);
  GreenBlock.sprite  = Sprite(55,0,BLOCK_WIDTH,BLOCK_HEIGHT,src);
  PinkBlock.sprite   = Sprite(66,0,BLOCK_WIDTH,BLOCK_HEIGHT,src);
}

Tetromino::Tetromino(int type) : grid(NULL) {
  if      (type == TETROMINO_L)
    genL();
  else if (type == TETROMINO_REVERSED_L)
    genRevL();
  else if (type == TETROMINO_Z)
    genZ();
  else if (type == TETROMINO_REVERSED_Z)
    genRevZ();
  else if (type == TETROMINO_T)
    genT();
  else if (type == TETROMINO_QUAD)
    genQuad();
  else if (type == TETROMINO_LONG)
    genLong();
}

void Tetromino::attachGrid(BlockGrid* grid_) {
  grid = grid_;
}

void Tetromino::setPosition(int x_, int y_) {
  x = x_;
  y = y_;
}

std::vector<Block> Tetromino::getBlocks() const {
  std::vector<Block> v(4);
  for (size_t i = 0; i < v.size(); ++i) {
    v[i] = blocks[i];
    v[i].x = x + blocks[i].x;
    v[i].y = y + blocks[i].y;
  }
  return v;
}

std::vector<Block> Tetromino::getMoveRight() const {
  std::vector<Block> v(4);
  for (size_t i = 0; i < v.size(); ++i) {
    v[i] = blocks[i];
    v[i].x = x+1 + blocks[i].x;
    v[i].y = y   + blocks[i].y;
  }
  return v;
}

std::vector<Block> Tetromino::getMoveLeft () const {
  std::vector<Block> v(4);
  for (size_t i = 0; i < v.size(); ++i) {
    v[i] = blocks[i];
    v[i].x = x-1 + blocks[i].x;
    v[i].y = y   + blocks[i].y;
  }
  return v;
}

std::vector<Block> Tetromino::getMoveDown () const {
  std::vector<Block> v(4);
  for (size_t i = 0; i < v.size(); ++i) {
    v[i] = blocks[i];
    v[i].x = x   + blocks[i].x;
    v[i].y = y-1 + blocks[i].y;
  }
  return v;
}

std::vector<Block> Tetromino::getTurnRight() const {
  std::vector<Block> v(4);
  // When turn right: x = y, y = -x
  for (size_t i = 0; i < v.size(); ++i) {
    v[i] = blocks[i];
    v[i].x = x + blocks[i].y;
    v[i].y = y - blocks[i].x;
  }
  return v;
}

std::vector<Block> Tetromino::getTurnLeft () const {
  std::vector<Block> v(4);
  // When turn left: x = -y, y = x
  for (size_t i = 0; i < v.size(); ++i) {
    v[i] = blocks[i];
    v[i].x = x - blocks[i].y;
    v[i].y = y + blocks[i].x;
  }
  return v;
}

bool Tetromino::moveRight() {
  if (grid == NULL)
    return false;

  if (grid->validMove(getBlocks(), getMoveRight())) {
    grid->moveBlocks(getBlocks(), getMoveRight());
    x += 1;
    return true;
  }
  return false;
}

bool Tetromino::moveLeft () {
  if (grid == NULL)
    return false;

  if (grid->validMove(getBlocks(), getMoveLeft())) {
    grid->moveBlocks(getBlocks(), getMoveLeft());
    x -= 1;
    return true;
  }
  return false;
}

bool Tetromino::moveDown () {
  if (grid == NULL)
    return false;

  if (grid->validMove(getBlocks(), getMoveDown())) {
    grid->moveBlocks(getBlocks(), getMoveDown());
    y -= 1;
    return true;
  }
  return false;
}

bool Tetromino::turnRight() {
  if (grid == NULL)
    return false;

  if (grid->validMove(getBlocks(), getTurnRight())) {
    grid->moveBlocks(getBlocks(), getTurnRight());
    for (int i = 0; i < 4; ++i) {
      int newx =  blocks[i].y;
      int newy = -blocks[i].x;
      blocks[i].x = newx;
      blocks[i].y = newy;
    }
    return true;
  }
  return false;
}

bool Tetromino::turnLeft () {
  if (grid == NULL)
    return false;

  if (grid->validMove(getBlocks(), getTurnLeft())) {
    grid->moveBlocks(getBlocks(), getTurnLeft());
    for (int i = 0; i < 4; ++i) {
      int newx = -blocks[i].y;
      int newy =  blocks[i].x;
      blocks[i].x = newx;
      blocks[i].y = newy;
    }
    return true;
  }
  return false;
}

void Tetromino::genL() {
  // (0,0) in the corner of the L
  blocks[0].x = 0;
  blocks[0].y = 0;

  blocks[1].x = -1;
  blocks[1].y =  0;

  blocks[2].x = -2;
  blocks[2].y =  0;

  blocks[3].x = 0;
  blocks[3].y = 1;

  assignColor(PurpleBlock);
}

void Tetromino::genRevL() {
  // (0,0) in the corner of the L
  blocks[0].x = 0;
  blocks[0].y = 0;

  blocks[1].x = 1;
  blocks[1].y = 0;

  blocks[2].x = 2;
  blocks[2].y = 0;

  blocks[3].x = 0;
  blocks[3].y = 1;

  assignColor(GreenBlock);
}

void Tetromino::genZ() {
  // (0,0) in a corner of the Z
  blocks[0].x = 0;
  blocks[0].y = 0;

  blocks[1].x = 1;
  blocks[1].y = 0;

  blocks[2].x = 0;
  blocks[2].y = 1;

  blocks[3].x = -1;
  blocks[3].y =  1;

  assignColor(RedBlock);
}

void Tetromino::genRevZ() {
  // (0,0) in a corner of the Z
  blocks[0].x = 0;
  blocks[0].y = 0;

  blocks[1].x = -1;
  blocks[1].y =  0;

  blocks[2].x = 0;
  blocks[2].y = 1;

  blocks[3].x = 1;
  blocks[3].y = 1;

  assignColor(OrangeBlock);
}

void Tetromino::genT() {
  // (0,0) in intersection of T
  blocks[0].x = 0;
  blocks[0].y = 0;

  blocks[1].x = 1;
  blocks[1].y = 0;

  blocks[2].x = -1;
  blocks[2].y =  0;

  blocks[3].x = 0;
  blocks[3].y = 1;

  assignColor(PinkBlock);
}

void Tetromino::genQuad() {
  // (0,0) in the bottom-left corner
  blocks[0].x = 0;
  blocks[0].y = 0;

  blocks[1].x = 1;
  blocks[1].y = 0;

  blocks[2].x = 0;
  blocks[2].y = 1;

  blocks[3].x = 1;
  blocks[3].y = 1;

  assignColor(YellowBlock);
}

void Tetromino::genLong() {
  // (0,0) in the second block
  blocks[0].x = -1;
  blocks[0].y = 0;

  blocks[1].x = 0;
  blocks[1].y = 0;

  blocks[2].x = 1;
  blocks[2].y = 0;

  blocks[3].x = 2;
  blocks[3].y = 0;

  assignColor(BlueBlock);
}

void Tetromino::assignColor(Block b) {
  for (int i = 0; i < 4; ++i)
    blocks[i].sprite = b.sprite;
}