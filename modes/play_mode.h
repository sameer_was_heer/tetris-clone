#ifndef PLAY_MODE_H
#define PLAY_MODE_H

#include <string>
#include <vector>
#include "engine/time_manager.h"
#include "engine/game_mode.h"
#include "engine/random.h"
#include "tetromino.h"

struct Block;

class BlockGrid {
 public:
  BlockGrid();
  ~BlockGrid() { }

  void draw();
  int drawWidth() const;
  int drawHeight() const;

  bool empty(int x, int y) const;
  bool insert(Tetromino& t); // Returns false when the respawn zone is occupied
  int checkLines(Tetromino& t);
  void moveBlocks(const std::vector<Block>& src, const std::vector<Block>& dst);
  bool validMove(const std::vector<Block>& src, const std::vector<Block>& dst) const;

  int width () const { return grid[0].size(); }
  int height() const { return grid.size();    }

 private:
  bool lineCompleted(int y);
  void removeLine(int y);
  bool isInside(const Block& b, const std::vector<Block>& dst) const;
  void drawBorders();
  void drawIntersections();

  std::vector<std::vector<Block> > grid;
};

class PlayMode : public GameMode {
 public:
  PlayMode();
  ~PlayMode();

  void enter();
  void pause();
  void resume();

  void update();
  void draw();
  
 private:
  void generateNextTetromino();
  void respawnNextTetromino();
  void turnRight();
  void moveRight();
  void moveLeft();
  void moveDown();
  void addScore(int lines);
  std::string getStringScore();
  void drawNextTetromino();
  void drawScore();

  BlockGrid grid;
  Tetromino current, next;
  int score;

  Random random;
  Timer timer;
};

#endif