#ifndef GAME_STOPPED_H
#define GAME_STOPPED_H

#include "engine/game_mode.h"

class GameStopped : public GameMode {
 protected:
  GameStopped(GameMode* parent_) : parent(parent_) { }
  void shadowScreen();

  GameMode* parent;
};

class GameOver : public GameStopped {
 public:
  GameOver(GameMode* parent_) : GameStopped(parent_) { }

  void update();
  void draw();
};

class GamePaused : public GameStopped {
 public:
  GamePaused(GameMode* parent_) : GameStopped(parent_) { }

  void update();
  void draw();
};

#endif