#ifndef FONT_H
#define FONT_H

#include <string>
#include "engine/video/sprite.h"

// 26 letters + 10 numbers + colon + SPACE
#define CHAR_TOTAL 36

class Font {
 public:
  static void init(const Sprite& src);
  static void draw(const std::string& text, int x, int y);

 private:
  static int getChar(char c);
  static Sprite chars[CHAR_TOTAL];
};

#endif