#ifndef CONSTANTS_H
#define CONSTANTS_H

const int PLAY_MODE_REPEAT_DELAY = 100;

const int PLAY_ZONE_WIDTH  = 171;
const int PLAY_ZONE_HEIGHT = 201;

const int ZOOM          = 2;
const int WINDOW_WIDTH  = PLAY_ZONE_WIDTH  * ZOOM;
const int WINDOW_HEIGHT = PLAY_ZONE_HEIGHT * ZOOM;

const int BLOCK_WIDTH   =  10;
const int BLOCK_HEIGHT  =  10;
const int BLOCK_FALL_MS = 1000; // time the block waits until the next step down

const int LINE_SCORE = 100; // score of each line
const int LINE_BONUS =  50; // extra score for each line (only if lines > 1)

const int GRID_WIDTH  = 10;
const int GRID_HEIGHT = 22;

const int RESPAWN_X = 4;
const int RESPAWN_Y = 20;

const int SCORE_TEXT_DRAW_POS_X     = 11 * BLOCK_WIDTH;
const int SCORE_TEXT_DRAW_POS_Y     = 14 * BLOCK_HEIGHT;
const int SCORE_DRAW_POS_X          = 11 * BLOCK_WIDTH;
const int SCORE_DRAW_POS_Y          = 13 * BLOCK_HEIGHT;
const int NEXT_TETROMINO_DRAW_POS_X = 13;
const int NEXT_TETROMINO_DRAW_POS_Y = 17;
const int GAME_OVER_DRAW_POS_X      = PLAY_ZONE_WIDTH  / 4;
const int GAME_OVER_DRAW_POS_Y      = PLAY_ZONE_HEIGHT / 2;

#endif