#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "time_manager.h"

class InputHandler;
extern InputHandler* InputManager;

// These defines are for improve the readablity and understandability
// of the code.
#define PRESSED  true
#define RELEASED false

// These defines are for use in the sticky param. Improve readablity and
// understandability. (NO_STICKY is not necessary because is the default
// behavior)
#define NO_STICKY 0
#define STICKY    1
#define REPEAT    2

// To reuse the InputHandler, redefine the Keys enum and change the
// implementation of the init method
enum Keys {
  KEY_UP,
  KEY_DOWN,
  KEY_LEFT,
  KEY_RIGHT,
  KEY_PAUSE,
  KEY_EXIT,

  // Total of keys. Used in the InputHandler
  KEY_TOTAL = 6
};

class InputHandler {
 public:
  InputHandler();
  ~InputHandler();

  void update();

  // NO_STICKY means the function will return the real state of the key at that
  // moment.
  // STICKY means the function only will return PRESSED the first time you
  // check the state of a PRESSED key.
  // REPEAT is like STICKY, but after "Repeat Wait" ms, the key will return
  // pressed again, and after this second time it will return PRESSED again
  // everytime "Repeat Delay" ms has passed.
  bool isPressed(Keys key, int sticky = NO_STICKY);
  // By default, 1 second
  void setRepeatWait(int rep_wait) { repeat_wait = rep_wait; }
  // By default, 0.2 seconds
  void setRepeatDelay(int rep_delay) { repeat_delay = rep_delay; }

 private:
  void init();

  struct KeyState {
    KeyState() : sdlKey(0), state(RELEASED), realstate(RELEASED), repeat(false)
      { }
    int sdlKey;
    Timer repeat_timer;
    bool state, realstate, repeat, unused; //unused is for data alignment
  };

  KeyState keys[KEY_TOTAL];
  int repeat_wait; // time between the first press and the repeated presses.
  int repeat_delay; // time between a repeat and another repeat.
};

#endif