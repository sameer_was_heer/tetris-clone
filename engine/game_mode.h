#ifndef GAME_MODE_H
#define GAME_MODE_H

class GameMode {
 public:
  GameMode() { }
  virtual ~GameMode() { }

  // Called when the mode enters or exits de ModeManager
  virtual void enter() { }
  virtual void exit() { }

  // Called when the mode is paused or resumed (active mode changes)
  virtual void pause() { }
  virtual void resume() { }

  virtual void update() = 0;
  virtual void draw() = 0;
};

#endif