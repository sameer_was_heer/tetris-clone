#include "mode_manager.h"
#include "game_mode.h"
#include "log.h"

ModeHandler* ModeManager = NULL;

ModeHandler::ModeHandler() {
  if (ModeManager == NULL)
    ModeManager = this;
  else {
    FILE_LOG(logWARNING) << "The Mode Manager is already started";
    return;
  }
}

ModeHandler::~ModeHandler() {
  if (ModeManager == this)
    ModeManager = NULL;

  // Clean non-pushed modes
  while (!changes.empty()) {
    GameMode* mode = changes.front();
    if (mode != NULL)
      delete mode;
    changes.pop_front();
  }

  // Clean pushed modes
  while (!modes.empty()) {
    GameMode* mode = modes.top();
    mode->exit();
    delete mode;
    modes.pop();
  }
}

void ModeHandler::push(GameMode* mode) {
  if (mode != NULL)
    changes.push_back(mode);
  else
    FILE_LOG(logWARNING) << "Pushing a non-initialized mode on the stack";
}

void ModeHandler::pop() {
  changes.push_front(NULL);
}

void ModeHandler::update() {
  updateStack();
  if (modes.empty()) {
    FILE_LOG(logERROR) << "No modes remaining after update the mode stack";
    return;
  }

  GameMode* mode = modes.top();
  mode->update();
}

void ModeHandler::draw() {
  if (modes.empty()) {
    FILE_LOG(logERROR) << "No modes remaining in the mode stack";
    return;
  }

  GameMode* mode = modes.top();
  mode->draw();
}

void ModeHandler::updateStack() {
  while (!changes.empty()) {
    GameMode* mode = changes.front();
    if (mode != NULL)
      insertMode();
    else
      deleteMode();
    changes.pop_front();
  }
}

void ModeHandler::insertMode() {
  if (!modes.empty()) {
    GameMode* prev_mode = modes.top();
    prev_mode->pause();
  }

  GameMode* mode = changes.front();
  modes.push(mode);
  mode->enter();
}

void ModeHandler::deleteMode() {
  if (modes.empty()) {
    FILE_LOG(logERROR) << "Can't pop in the mode stack. The stack is empty";
    return;
  }

  GameMode* mode = modes.top();
  modes.pop();
  mode->exit();
  delete mode;

  if (!modes.empty()) {
    GameMode* prev_mode = modes.top();
    prev_mode->resume();
  }
}