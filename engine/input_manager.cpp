#include "input_manager.h"
#include <SDL/SDL.h>
#include "system_manager.h"
#include "log.h"

InputHandler* InputManager = NULL;

InputHandler::InputHandler() : repeat_wait(1000), repeat_delay(200) {
  if (InputManager == NULL)
    InputManager = this;
  else {
    FILE_LOG(logWARNING) << "The Input Manager is already started";
    return;
  }

  init();
}

InputHandler::~InputHandler() {
  if (InputManager == this)
    InputManager = NULL;
}

void InputHandler::update() {
  SDL_Event event;
  while (SDL_PollEvent(&event) != 0) {
    switch(event.type) {
      case SDL_KEYDOWN:
      case SDL_KEYUP:
        for (int i = 0; i < KEY_TOTAL; ++i)
          if (keys[i].sdlKey == event.key.keysym.sym) {
            keys[i].state = keys[i].realstate =
              event.type == SDL_KEYDOWN? PRESSED : RELEASED;
            keys[i].repeat = false; // stop repeating on a state change
          }
        break;
      case SDL_QUIT:
        SystemManager->stop();
    }
  }
}

bool InputHandler::isPressed(Keys key, int sticky) {
  keys[key].repeat_timer.update();
  switch(sticky) {
    case NO_STICKY:
      return keys[key].realstate;

    case STICKY:
      {
        bool keystate = keys[key].state;
        keys[key].state = RELEASED;
        return keystate;
      }

    case REPEAT:
      if (keys[key].repeat) {
        if (keys[key].repeat_timer.isFinished() && keys[key].realstate) {
          keys[key].repeat_timer.setDuration(repeat_delay);
          keys[key].repeat_timer.reset();
          keys[key].state = PRESSED;
        }
      }
      else {
        keys[key].repeat = true;
        keys[key].repeat_timer.setDuration(repeat_wait);
        keys[key].repeat_timer.reset();
        keys[key].state = keys[key].realstate;
      }
      {
        bool keystate = keys[key].state;
        keys[key].state = RELEASED;
        return keystate;
      }
  }
  return false;
}

void InputHandler::init() {
  keys[KEY_UP].sdlKey    = SDLK_UP;
  keys[KEY_DOWN].sdlKey  = SDLK_DOWN;
  keys[KEY_LEFT].sdlKey  = SDLK_LEFT;
  keys[KEY_RIGHT].sdlKey = SDLK_RIGHT;
  keys[KEY_PAUSE].sdlKey = SDLK_RETURN;
  keys[KEY_EXIT].sdlKey  = SDLK_ESCAPE;

  for (int i = 0; i < KEY_TOTAL; ++i)
    keys[i].repeat_timer.run();
}