#ifndef TIME_MANAGER_H
#define TIME_MANAGER_H

#include <stdint.h>
#include <list>

class TimeHandler;
extern TimeHandler* TimeManager;

// All the timers starts in paused state
class Timer {
 public:
  Timer(bool autoupdate_ = false);
  Timer(int duration_, bool autoupdate_ = false);
  ~Timer();

  void setAutoUpdate();
  // Reset the timer but not modifies its state (paused/resumed)
  void reset();
  void run();
  void pause();

  // Returns true if the timer finished
  void update();
  // Waits until the timer finishes
  void wait();
  bool isFinished() { return finished; }

  int  getDuration() { return duration; }
  void setDuration(int duration_) { duration = duration_; }

 private:
  // Return the number of miliseconds passed since the start of the engine
  uint32_t getTime();
  void defaultValues();

  uint16_t duration, time_passed;
  uint32_t last_update;
  bool autoupdate, running, finished, unused; // unused is for data align
};

class TimeHandler {
 public:
  TimeHandler();
  ~TimeHandler();

  // Update all the timers in autoupdate mode.
  void update();

 private:
  friend class Timer;
  typedef std::list<Timer*>::iterator ListIterator;

  void addTimer(Timer* timer);
  void removeTimer(Timer* timer);
  
  std::list<Timer*> timers;
};

#endif