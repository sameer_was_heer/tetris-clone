#ifndef MODE_MANAGER_H
#define MODE_MANAGER_H

#include <deque>
#include <stack>

class ModeHandler;
class GameMode;
extern ModeHandler* ModeManager;

class ModeHandler {
 public:
  ModeHandler();
  ~ModeHandler();

  // All the GameMode pushed inside must be created with de new operator.
  // The ModeHandler will release the memory automatically.
  void push(GameMode* mode);
  void pop();

  // update must be called before draw on the main loop
  void update();
  void draw();

 private:
  void updateStack();
  void insertMode();
  void deleteMode();

  std::stack<GameMode*> modes;
  // Only one queue for changes. Pushes are inserted at the end and pops
  // are inserted at the beggining as null modes
  std::deque<GameMode*> changes;
};

#endif