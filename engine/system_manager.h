#ifndef SYSTEM_MANAGER_H
#define SYSTEM_MANAGER_H

class SystemHandler;
extern SystemHandler* SystemManager;

class SystemHandler {
 public:
  SystemHandler();
  ~SystemHandler();

  bool isFinished();
  void stop() { stop_request = true; }
  void sleep(int ms);

 private:
  bool stop_request;
};

#endif