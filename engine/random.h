#ifndef PRNG_MARSENNE_TWISTER_H
#define PRNG_MARSENNE_TWISTER_H

// Pseudo-random number generator that uses the Marsenne twister algorithm in
// the MT19937 version
class Random {
 public:
  Random() : index(0) { }
  Random(unsigned int seed_);
  ~Random() { }

  void randomize();
  void setSeed(unsigned int seed_);
  unsigned int getSeed() { return seed; }

  // Return an int in the range [0, 2^32)
  // The range becomes [-2^31, 2^31) for signed ints
  unsigned int getInt();
  // Return an int in the range [0, max)
  unsigned int getInt(int max);
  // Return an int in the range [min, max)
  unsigned int getInt(int min, int max);

  // Return a float in the range [0, 1)
  float getFloat();
  // Return a float in the range [0, max)
  float getFloat(float max);
  // Return a float in the range [min, max)
  float getFloat(float min, float max);

  // Return a double in the range [0, 1)
  double getDouble();
  // Return a double in the range [0, max)
  double getDouble(double max);
  // Return a double in the range [min, max)
  double getDouble(double min, double max);

  // Return a random boolean
  bool getBool();

 private:
  static const int MTsize = 624;
  void init();
  void generateNumbers();
  unsigned int extractNumber();

  unsigned int MT[MTsize];
  unsigned int seed;
  int index;
};

#endif