#include "time_manager.h"
#include <SDL/SDL.h>
#include "system_manager.h"
#include "log.h"

TimeHandler* TimeManager = NULL;

Timer::Timer(bool autoupdate_) {
  defaultValues();
  if (autoupdate_)
    setAutoUpdate();
}

Timer::Timer(int duration_, bool autoupdate_) {
  defaultValues();
  setDuration(duration_);
  if (autoupdate_)
    setAutoUpdate();
}

Timer::~Timer() {
  if (autoupdate && TimeManager != NULL)
    TimeManager->removeTimer(this);
}

void Timer::setAutoUpdate() {
  if (TimeManager != NULL) {
    TimeManager->addTimer(this);
    autoupdate = true;
  }
}

void Timer::reset() {
  time_passed = 0;
  finished = false;
  last_update = getTime();
}

void Timer::run() {
  running = true;
  last_update = getTime();
}

void Timer::pause() {
  running = false;
}

void Timer::update() {
  if (!running || finished)
    return;

  uint32_t now = getTime();
  time_passed += now - last_update;
  if (time_passed >= duration)
    finished = true;
  last_update = now;
}

void Timer::wait() {
  running = true;
  if (!isFinished())
    SystemManager->sleep(duration - time_passed);
  update();
}

uint32_t Timer::getTime() {
  return SDL_GetTicks();
}

void Timer::defaultValues() {
  duration = time_passed = 0;
  last_update = 0;
  autoupdate = running = finished = false;
}

TimeHandler::TimeHandler() {
  if (TimeManager == NULL)
    TimeManager = this;
  else {
    FILE_LOG(logWARNING) << "The Time Manager is already started";
    return;
  }
}

TimeHandler::~TimeHandler() {
  if (TimeManager == this)
    TimeManager = NULL;
}

void TimeHandler::update() {
  for (ListIterator it = timers.begin(); it != timers.end(); ++it)
    (*it)->update();
}

void TimeHandler::addTimer(Timer* timer) {
  timers.push_back(timer);
}

void TimeHandler::removeTimer(Timer* timer) {
  timers.remove(timer);
}