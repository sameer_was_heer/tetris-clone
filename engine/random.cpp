#include "random.h"

#include <stdlib.h>
#include <time.h>

Random::Random(unsigned int seed_) : seed(seed_), index(0) {
  init();
}

void Random::randomize() {
  srand(time(NULL));
  setSeed(rand());
}

void Random::setSeed(unsigned int seed_) {
  seed = seed_;
  init();
}

void Random::init() {
  MT[0] = seed;
  for (int i = 1; i < MTsize; ++i)
    MT[i] = 0x6c078965 * (MT[i-1] ^ (MT[i-1]>>30)) + i;
}

void Random::generateNumbers() {
  for (int i = 0; i < MTsize; ++i) {
    unsigned int y = (MT[i] & 0x80000000) + (MT[(i+1)%MTsize] & 0x7fffffff);
    if (y%2 != 0)
      MT[i] = MT[i] ^ 0x9908b0df;
  }
}

unsigned int Random::extractNumber() {
  if (index == 0)
    generateNumbers();

  unsigned int y = MT[index];
  y ^= y>>11;
  y ^= y<< 7 & 0x9d2c5680;
  y ^= y<<15 & 0xefc60000;
  y ^= y>>18;
  index = (index+1)%MTsize;
  return y;
}

unsigned int Random::getInt() {
  return extractNumber();
}

unsigned int Random::getInt(int max) {
  return getFloat() * max;
  // return getInt() % (max != 0? max : 1);
}

unsigned int Random::getInt(int min, int max) {
  int range = max - min;
  return min + getFloat() * range;
}

float Random::getFloat() {
  float randMax = 4294967296.0f;
  return getInt() / randMax;
}

float Random::getFloat(float max) {
  return getFloat() * max;
}

float Random::getFloat(float min, float max) {
  float range = max - min;
  return min + getFloat() * range;
}

double Random::getDouble() {
  double randMax = 4294967296.0;
  return getInt() / randMax;
}

double Random::getDouble(double max) {
  return getDouble() * max;
}

double Random::getDouble(double min, double max) {
  double range = max - min;
  return min + getDouble() * range;
}

bool Random::getBool() {
  return getInt(2);
}
