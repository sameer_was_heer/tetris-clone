#include "system_manager.h"
#include <SDL/SDL.h>
#include "log.h"

SystemHandler* SystemManager = NULL;

SystemHandler::SystemHandler() : stop_request(false) {
  if (SystemManager == NULL)
    SystemManager = this;
  else {
    FILE_LOG(logWARNING) << "The System Manager is already started";
    return;
  }
}

SystemHandler::~SystemHandler() {
  if (SystemManager == this)
    SystemManager = NULL;
}

bool SystemHandler::isFinished() {
  return stop_request;
}

void SystemHandler::sleep(int ms) {
  SDL_Delay(ms);
}