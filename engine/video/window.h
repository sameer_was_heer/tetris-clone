/*
 *  File written by Daniel Galacho on November 2012
 */

#ifndef WINDOW_H
#define WINDOW_H

#include <stdint.h>
#include <string>
#include "camera.h"

class Window;
extern Window* AppWindow;

class Window {
public:
  Window(int w, int h, bool fullscreen = false);
  ~Window();

  Camera* getCamera() { return &camera; }
  int getWidth () const { return width;  }
  int getHeight() const { return height; }

  void setTitle(const std::string& title);
  void clear()  const;
  void update() const;

private:
  void setWidth (int w) { width  = w; }
  void setHeight(int h) { height = h; }

  int width, height;
  Camera camera;
};

#endif