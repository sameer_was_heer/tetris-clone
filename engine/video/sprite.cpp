/*
 *  File written by Daniel Galacho on November 2012
 */

#include "sprite.h"
#include <stdlib.h>
#include <SDL/SDL_image.h>
#include "../log.h"

Sprite::Sprite() {
  defaultValues();
}

Sprite::Sprite(const std::string& filename, bool smooth) {
  defaultValues();
  load(filename, smooth);
  if (!initialized())
    dereference();

  width  = tex->width;
  height = tex->height;

  // Maps all the texture
  tex_xmin = 0.0f;
  tex_xmax = 1.0f;
  tex_ymin = 0.0f;
  tex_ymax = 1.0f;
}

Sprite::Sprite(int x, int y, int w, int h, const std::string& filename, bool smooth) {
  defaultValues();
  Sprite image_sprite(filename, smooth);
  *this = Sprite(x, y, w, h, image_sprite);
}

Sprite::Sprite(int x, int y, int w, int h, const Sprite& sprite) {
  defaultValues();
  *this = sprite;

  int tex_width  = getTextureWidth ();
  int tex_height = getTextureHeight();

  if (tex_width != 0.0f && tex_height != 0.0f) {
    tex_xmin = static_cast<float>(x)   / static_cast<float>(tex_width);
    tex_xmax = static_cast<float>(x+w) / static_cast<float>(tex_width);
    tex_ymin = static_cast<float>(y)   / static_cast<float>(tex_height);
    tex_ymax = static_cast<float>(y+h) / static_cast<float>(tex_height);
  }

  checkOutOfBounds();

  width  = w;
  height = h;
}

Sprite::~Sprite() {
  dereference();
}

Sprite::Sprite(const Sprite& sprite) {
  defaultValues();
  *this = sprite;
}

Sprite& Sprite::operator=(const Sprite& sprite) {
  if (this == &sprite)
    return *this;

  dereference();
  tex = sprite.tex;
  if (tex != NULL)
    ++(tex->reference_count);

  tex_xmin = sprite.tex_xmin;
  tex_xmax = sprite.tex_xmax;
  tex_ymin = sprite.tex_ymin;
  tex_ymax = sprite.tex_ymax;

  width = sprite.width;
  height = sprite.height;

  red   = sprite.red;
  green = sprite.green;
  blue  = sprite.blue;
  alpha = sprite.alpha;

  return *this;
}

void Sprite::draw(int x, int y, int w, int h, float angle, bool center) const {
  if (!initialized())
    return;

  glPushMatrix();

  glTranslatef(x, y, 0.0f);
  glRotatef(angle, 0.0f, 0.0f, 1.0f);
  glColor4ub(red, green, blue, alpha);
  glBindTexture(GL_TEXTURE_2D, tex->image);

  glBegin(GL_QUADS);
  if (center) {
    int x_radius = w >> 1;
    int y_radius = h >> 1;
    // Bottom-left vertex
    glTexCoord2f(tex_xmin, tex_ymax);
    glVertex3i(-x_radius, -y_radius, 0);
    // Top-left vertex
    glTexCoord2f(tex_xmin, tex_ymin);
    glVertex3i(-x_radius,  y_radius, 0);
    // Top-right vertex
    glTexCoord2f(tex_xmax, tex_ymin);
    glVertex3i( x_radius,  y_radius, 0);
    // Bottom-right vertex
    glTexCoord2f(tex_xmax, tex_ymax);
    glVertex3i( x_radius, -y_radius, 0);
  }
  else {
    // Bottom-left vertex
    glTexCoord2f(tex_xmin, tex_ymax);
    glVertex3i(0, 0, 0);
    // Top-left vertex
    glTexCoord2f(tex_xmin, tex_ymin);
    glVertex3i(0, h, 0);
    // Top-right vertex
    glTexCoord2f(tex_xmax, tex_ymin);
    glVertex3i(w, h, 0);
    // Bottom-right vertex
    glTexCoord2f(tex_xmax, tex_ymax);
    glVertex3i(w, 0, 0);
  }
  glEnd();

  glPopMatrix();
}

void Sprite::load(const std::string& filename, bool smooth) {
  if (tex == NULL)
    tex = new Texture;

  SDL_Surface* image = IMG_Load(filename.c_str());
  if (image == NULL) {
    FILE_LOG(logWARNING) << "Can't load " << filename;
    return;
  }

  tex->width  = image->w;
  tex->height = image->h;
  if (tex->width & (tex->width-1)) {
    FILE_LOG(logWARNING) << "Width of " << filename << " is not a power of 2";
  }
  if (tex->height & (tex->height-1)) {
    FILE_LOG(logWARNING) << "Height of " << filename << " is not a power of 2";
  }

  bool has_alpha = image->format->BytesPerPixel == 4;
  GLenum color_type = has_alpha? GL_RGBA : GL_RGB;

  glGenTextures(1, &tex->image);
  glBindTexture(GL_TEXTURE_2D, tex->image);

  if (smooth) {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }
  else {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }

  glTexImage2D(GL_TEXTURE_2D, 0, has_alpha? GL_RGBA8 : GL_RGB8, tex->width,
               tex->height, 0, color_type, GL_UNSIGNED_BYTE, image->pixels);

  SDL_FreeSurface(image);
}

void Sprite::defaultValues() {
  tex = NULL;
  tex_xmin = tex_xmax = tex_ymin = tex_ymax = 0;
  width = height = 0;
  red = green = blue = alpha = 255;
}

void Sprite::dereference() {
  if (tex != NULL) {
    --(tex->reference_count);
    if (tex->reference_count == 0) {
      glDeleteTextures(1, &tex->image);
      delete tex;
    }
    tex = NULL;
  }
}

void Sprite::checkOutOfBounds() const {
  if (tex_xmin > 1.0f ||
      tex_xmax > 1.0f ||
      tex_ymin > 1.0f ||
      tex_ymax > 1.0f) {
    FILE_LOG(logWARNING) << "Sprite grater than texture";
  }
}