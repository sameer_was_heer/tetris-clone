/*
 *  File written by Daniel Galacho on November 2012
 */

#include "shapes.h"

#include <SDL/SDL_opengl.h>

void Colored::setColor(int r, int g, int b, int a) {
  red   = r;
  green = g;
  blue  = b;
  alpha = a;
}

void Point::draw() {
  Point::draw(x, y, red, green, blue, alpha);
}

void Point::draw(float x, float y, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
  glBindTexture(GL_TEXTURE_2D, 0);
  glColor4ub(r, g, b, a);
  glBegin(GL_POINTS);
    glVertex3f(x, y, 0);
  glEnd();
}

void Line::draw() {
  Line::draw(v0, v1, red, green, blue, alpha);
}

void Line::draw(Point v0, Point v1, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
  glBindTexture(GL_TEXTURE_2D, 0);
  glColor4ub(r, g, b, a);
  glBegin(GL_LINES);
    glVertex3f(v0.getX(), v0.getY(), 0);
    glVertex3f(v1.getX(), v1.getY(), 0);
  glEnd();
}

void Rect::draw() {
  Rect::draw(v0, v1, red, green, blue, alpha);
}

void Rect::draw(Point v0, Point v1, uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
  glBindTexture(GL_TEXTURE_2D, 0);
  glColor4ub(r, g, b, a);
  glBegin(GL_QUADS);
    glVertex3f(v0.getX(), v0.getY(), 0);
    glVertex3f(v1.getX(), v0.getY(), 0);
    glVertex3f(v1.getX(), v1.getY(), 0);
    glVertex3f(v0.getX(), v1.getY(), 0);
  glEnd();
}

void Rect::drawBorders() {
  Rect::drawBorders(v0, v1, red, green, blue, alpha);
}

void Rect::drawBorders(Point v0, Point v1,
                       uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
  glBindTexture(GL_TEXTURE_2D, 0);
  glColor4ub(r, g, b, a);
  glBegin(GL_LINE_LOOP);
    glVertex3f(v0.getX(), v0.getY(), 0);
    glVertex3f(v1.getX(), v0.getY(), 0);
    glVertex3f(v1.getX(), v1.getY(), 0);
    glVertex3f(v0.getX(), v1.getY(), 0);
  glEnd();
}