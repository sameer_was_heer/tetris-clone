/*
 *  File written by Daniel Galacho on November 2012
 */

#ifndef CAMERA_H
#define CAMERA_H

class Camera {
 public:
  Camera();
  ~Camera() { }

  void move(int x_, int y_);
  void set (int x_, int y_);
  void zoom(int z);

  int getX() const { return x; }
  int getY() const { return y; }

  void windowResize(int w, int h) const;

 private:
  int x, y;
};

#endif