/*
 *  File written by Daniel Galacho on November 2012
 */

#include "window.h"
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include "../log.h"

const int redColorDepth   = 8;
const int greenColorDepth = 8;
const int blueColorDepth  = 8;
const int alphaColorDepth = 8;
const int doubleBuffer    = 1;

const int NO_DEPTH_BUFFER   = 0;
const int NO_STENCIL_BUFFER = 0;

Window* AppWindow = NULL;

Window::Window(int w, int h, bool fullscreen) : width(w), height(h) {
  if (AppWindow == NULL)
    AppWindow = this;
  else {
    FILE_LOG(logWARNING) << "The App Window is already started";
    return;
  }

  if (SDL_Init(SDL_INIT_VIDEO) == -1) {
    FILE_LOG(logERROR) << "Can't init SDL";
    return;
  }

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE,     redColorDepth);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,   greenColorDepth);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,    blueColorDepth);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,   alphaColorDepth);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, doubleBuffer);

  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,   NO_DEPTH_BUFFER);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, NO_STENCIL_BUFFER);

  int flags = SDL_OPENGL;
  if (fullscreen)
    flags |= SDL_FULLSCREEN;
  const int bpp = 32;
  SDL_Surface* window_surface = SDL_SetVideoMode(w, h, bpp, flags);

  if (window_surface == NULL) {
    FILE_LOG(logERROR) << "Can't open the window";
    return;
  }
  SDL_FreeSurface(window_surface);

  width = w;
  height = h;

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glViewport(0, 0, width, height);
  camera.windowResize(width, height);
}

Window::~Window() {
  if (AppWindow == this)
    AppWindow = NULL;

  SDL_Quit();
}

void Window::setTitle(const std::string& title) {
  SDL_WM_SetCaption(title.c_str(), title.c_str());
}

void Window::clear()  const {
  glClear(GL_COLOR_BUFFER_BIT);
}

void Window::update() const {
  SDL_GL_SwapBuffers();
}