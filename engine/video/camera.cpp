/*
 *  File written by Daniel Galacho on November 2012
 */

#include "camera.h"
#include <math.h>
#include <SDL/SDL_opengl.h>
#include "window.h"

const int zNear = -1;
const int zFar  =  1;


Camera::Camera() : x(0), y(0)
{ }

void Camera::move(int x_, int y_) {
  glTranslatef(-x_, -y_, 0);
  x += x_;
  y += y_;
}

void Camera::set(int x_, int y_) {
  glLoadIdentity();
  move(x_, y_);
  x = x_;
  y = y_;
}

void Camera::zoom(int z) {
  glScalef(z, z, 0);
}

void Camera::windowResize(int w, int h) const {
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  // int width  = w >> 1;
  // int height = h >> 1;
  int width  = w;
  int height = h;
  // glOrtho(-width, width, -height, height, zNear, zFar);
  glOrtho(0, width, 0, height, zNear, zFar);
  glMatrixMode(GL_MODELVIEW);
}