/*
 *  File written by Daniel Galacho on November 2012
 */

#ifndef SPRITE_H
#define SPRITE_H

#include <stdint.h>
#include <string>
#include <SDL/SDL_opengl.h>

// These defines are for use in the center param of the draw function.
// Makes the function more readable and understandable.
// (CORNER isn't necessary to use because is the default behavior)
#define CORNER false
#define CENTER true

// These define are for use in the smoot param of the constructor.
// Make the constructor more readable and undestandable.
// (NO_SMOOTH isn't necessary to use because is the default behavior)
#define NO_SMOOTH false
#define SMOOTH    true

// Sprite class can store an image (in format PNG with color format RGB or RGBA)
// or a quad segment of the image.
// With the constructor with a Sprite in the param list, you can share the
// loaded image between both sprites, so the same image doesn't loads twice in
// memory.
class Sprite {
 public:
  Sprite();
  Sprite(const std::string& filename, bool smooth = false);
  Sprite(int x, int y, int w, int h, const std::string& filename, bool smooth = false);
  Sprite(int x, int y, int w, int h, const Sprite& sprite);
  ~Sprite();

  Sprite(const Sprite& sprite);
  Sprite& operator=(const Sprite& sprite);

  // If center = false, draw the sprite with the bottom-left corner on (x, y)
  // If center = true, draw the sprite with the center on (x, y)
  // The sprite will rotate around (x, y) in a counter-clockwise direction.
  // The angle must be in degrees.
  void draw(int x, int y, bool center = false) const { draw(x, y, width, height, 0.0f, center); }
  void draw(int x, int y, float angle, bool center = false) const { draw(x, y, width, height, angle, center); }
  void draw(int x, int y, int w, int h, bool center = false) const { draw(x, y, w, h, 0.0f, center); }
  void draw(int x, int y, int w, int h, float angle, bool center = false) const;

  int getWidth () const { return width;  }
  int getHeight() const { return height; }
  int getTextureWidth () const { if (tex != NULL) return tex->width;  return 0; }
  int getTextureHeight() const { if (tex != NULL) return tex->height; return 0; }

  uint8_t getRed  () const { return red;   }
  uint8_t getGreen() const { return green; }
  uint8_t getBlue () const { return blue;  }
  uint8_t getAlpha() const { return alpha; }

  void setRed  (uint8_t r) { red   = r; }
  void setGreen(uint8_t g) { green = g; }
  void setBlue (uint8_t b) { blue  = b; }
  void setAlpha(uint8_t a) { alpha = a; }

  void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255)
    { red = r; green = g; blue = b; alpha = a; }

  // Return true when the sprite is loaded with any image
  bool initialized() const { return tex != NULL && tex->image != 0; }

 private:
  struct Texture {
    // reference_count starts at 1 because when a sprite creates it's
    // referencing it
    Texture() : reference_count(1), image(0), width(0), height(0) { }
    int reference_count;
    GLuint image;
    uint16_t width, height;
  };

  void load(const std::string& filename, bool smooth);
  // Assign the default values to the sprite
  void defaultValues();
  void dereference();
  // Logs a warning if the sprite is greater than the texture
  void checkOutOfBounds() const;

  Texture* tex;
  float tex_xmin, tex_xmax, tex_ymin, tex_ymax;
  uint16_t width, height;
  uint8_t red, green, blue, alpha;
};

#endif