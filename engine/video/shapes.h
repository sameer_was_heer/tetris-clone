/*
 *  File written by Daniel Galacho on November 2012
 */

#ifndef SHAPES_H
#define SHAPES_H

#include <stdint.h>

class Colored {
 public:
  uint8_t getRed  () const { return red;   }
  uint8_t getGreen() const { return green; }
  uint8_t getBlue () const { return blue;  }
  uint8_t getAlpha() const { return alpha; }

  void setColor(int r, int g, int b, int a = 255);

 protected:
  Colored() : red(255), green(255), blue(255), alpha(255) { }
  uint8_t red, green, blue, alpha;
};

struct Point : public Colored {
 public:
  Point(float x_ = 0, float y_ = 0) : x(x_), y(y_) { }

  float getX() const { return x; }
  float getY() const { return y; }

  void draw();
  static void draw(float x, float y,
                   uint8_t r = 255, uint8_t g = 255,
                   uint8_t b = 255, uint8_t a = 255);

 private:
  float x, y;
};

class Line : public Colored {
 public:
  Line(Point v0_, Point v1_) : v0(v0_), v1(v1_) { }

  Point getV0() { return v0; }
  Point getV1() { return v1; }

  void draw();
  static void draw(Point v0, Point v1,
                   uint8_t r = 255, uint8_t g = 255,
                   uint8_t b = 255, uint8_t a = 255);

 private:
  Point v0, v1;
};

class Rect : public Colored {
 public:
  // v0 should be the bottom-left corner and v1 the top-right corner
  Rect(Point v0_, Point v1_) : v0(v0_), v1(v1_) { }

  Point getV0() { return v0; }
  Point getV1() { return v1; }

  void draw();
  static void draw(Point v0, Point v1,
                   uint8_t r = 255, uint8_t g = 255,
                   uint8_t b = 255, uint8_t a = 255);

  void drawBorders();
  static void drawBorders(Point v0, Point v1,
                          uint8_t r = 255, uint8_t g = 255,
                          uint8_t b = 255, uint8_t a = 255);

 private:
  Point v0, v1;
};

#endif