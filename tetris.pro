TEMPLATE := app
TARGET   := tetris

CONFIG += release
QT     -= gui core

unix:LIBS += -lSDL -lSDL_image -lGL

SOURCES += \
    main.cpp \
    engine/video/window.cpp   \
    engine/video/sprite.cpp   \
    engine/video/shapes.cpp   \
    engine/video/camera.cpp   \
    engine/time_manager.cpp   \
    engine/input_manager.cpp  \
    engine/system_manager.cpp \
    engine/random.cpp         \
    engine/mode_manager.cpp   \
                              \
    modes/play_mode.cpp       \
    modes/tetromino.cpp       \
    modes/game_stopped.cpp    \
    font.cpp

HEADERS += \
    engine/video/window.h   \
    engine/video/sprite.h   \
    engine/video/shapes.h   \
    engine/video/camera.h   \
    engine/time_manager.h   \
    engine/input_manager.h  \
    engine/system_manager.h \
    engine/mode_manager.h   \
    engine/random.h         \
    engine/game_mode.h      \
    engine/log.h            \
                            \
    modes/play_mode.h       \
    modes/tetromino.h       \
    modes/game_stopped.h    \
    constants.h             \
    font.h
